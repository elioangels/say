#!/usr/bin/env node
"use strict"
const pkg = require("./package.json")
const say = require(".")

require("taketalk")({
  init(input, options) {
    console.log(say(input, options))
  },
  help() {
    console.log(`
  ${pkg.description}

  Usage
    $ say <string>
    $ say <string> --maxLength 8
    $ echo <string> | say

  Example
    $ say 'Sindre is a horse'
    ${say("Sindre is a horse")}`)
  },
  version: pkg.version,
})
