![](https://elioway.gitlab.io/elioangels/say/elio-say-logo.png)

> Don't reinvent the wheel... unless you want to know how the wheel works., **elioWay**

# say ![release](https://elioway.gitlab.io/eliosin/icon/devops/release/favicon.ico "release")

Like [cowsay](https://en.wikipedia.org/wiki/Cowsay) but more elioWay.

- [say Documentation](https://elioway.gitlab.io/elioangels/say)

## Installing

```
npm install @elioway/say
or
yarn add @elioway/say
```

- [Installing say](https://elioway.gitlab.io/elioangels/say/installing.html)

## Seeing is Believing

```
npm i
node say.js
```

## Nutshell

```
<pre>       _ _    _______ _     _
      | (_)  |__   __| |   (_)
   ___| |_  ___ | |  | |__  _ _ __   __ _
  / _ \ | |/ _ \| |  | '_ \| | '_ \ / _` |
 |  __/ | | (_) | |  | | | | | | | | (_| |
  \___|_|_|\___/|_|  |_| |_|_|_| |_|\__, |
                                     __/ |
                                    |___/ </pre>
```

- [say Quickstart](https://elioway.gitlab.io/elioangels/say/quickstart.html)
- [say Credits](https://elioway.gitlab.io/elioangels/say/credits.html)

![](https://elioway.gitlab.io/elioangels/say/apple-touch-icon.png)

## License

[BSD-2-Clause © Google](license) [Tim Bushell](mailto:theElioWay@gmail.com)
