# say Credits

## Core! Thanks

- [yeoman:yosay](https://github.com/yeoman/yosay)
- [coolgenerator](https://www.coolgenerator.com/ascii-text-generator)
- [Yann-Wang/ascii-text-generator](https://github.com/Yann-Wang/ascii-text-generator)

## Artwork

- [wikimedia:Annunciation](https://commons.wikimedia.org/wiki/File:Clevelandart_1950.251.jpg)
