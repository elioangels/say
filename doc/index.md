<aside>
  <dl>
  <dd>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
  Say first--for Heaven hides nothing from thy view,</dd>
  <dd>Nor the deep tract of Hell--say first what cause</dd>
</dl>
</aside>

This was a fun, early attempt at creating an **npm** module. I learned a lot about `package.json` and the **npm** CLI.

This is what **elioWay** apps are all about - tinkering; seeing if you can do it; learning; having fun. If there was ever an **elioWay** module which encompasses what we're about, it's this one.

Don't reinvent the wheel... unless you want to know how the wheel works.
