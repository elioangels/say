# say Quickstart

- [say Prerequisites](/elioangels/say/prerequisites.html)
- [Installing say](/elioangels/say/installing.html)

## Nutshell

1. Install and import into your javascript modules.
2. Call `say`

## Usage

```javascript
const say = require("say")

console.log(say("Tell Yeoman what to say **the elioWay**."))
```

Outputs:

```
      _ _
     | (_)
  ___| |_  ___    +--------------------------+
 / _ \ | |/ _ \   |  Tell Yeoman what to say |
|  __/ | | (_) |  |       **the elioWay**.       |
 \___|_|_|\___/   +--------------------------+
```

## CLI

```
npm install --global say
```

Help on command.

```
say --help
```

Usage on command.

```
say <string>
say <string> --maxLength 8
echo <string> | say
```

Example:

```
say 'Fork of yosay'
```

Outputs

```
      _ _
     | (_)
  ___| |_  ___    +--------------------------+
 / _ \ | |/ _ \   |      Fork of yosay       |
|  __/ | | (_) |  +--------------------------+
 \___|_|_|\___/
```
