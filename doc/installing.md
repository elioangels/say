# Installing say

- [say Prerequisites](/elioangels/say/prerequisites.html)

## npm

Install into your SASS projects.

```
npm install @elioway/say
yarn add @elioway/say
```

Import the functions into your master scss file.

```
@import "@elioway/say/";
```
